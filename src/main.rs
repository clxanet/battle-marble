//! Battle Marble is a single player game where two heavliy armored and armed
//! marbles battle it out on the high seas.
//!
//! The game works standalone and on the web.
//! To build for the web, you need:
//!
//! ```text
//! rustup target install wasm32-unknown-unknown
//! cargo install wasm-server-runner
//! ```
//!
//! and then
//!
//! ```text
//! cargo run --target wasm32-unknown-unknown
//! ```
//!

use std::f32::consts::PI;

use bevy::{app::PluginGroupBuilder, prelude::*};

/// The main entry point for any executable prepared a bevy `App`
/// and lets it run.
pub fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                fit_canvas_to_parent: true,
                prevent_default_event_handling: false,
                ..default()
            }),
            ..default()
        }))
        .add_plugins(GamePlugins::default())
        .run();
}

/// The in-game parts of the game include everything except
/// the title cards, and the main menu.
///
/// It also initializaes entities that life throughout the process,
/// such as the primary camera.
#[derive(Debug, Default)]
pub struct GamePlugins;

impl PluginGroup for GamePlugins {
    fn build(self) -> PluginGroupBuilder {
        PluginGroupBuilder::start::<Self>().add(SetupEternals)
    }
}

pub struct SetupEternals;

impl Plugin for SetupEternals {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, Self::setup);
    }
}

impl SetupEternals {
    fn setup(
        mut commands: Commands,
        mut meshes: ResMut<Assets<Mesh>>,
        mut materials: ResMut<Assets<StandardMaterial>>,
    ) {
        // create a mesh and material
        let mesh = meshes.add(shape::Icosphere::default().try_into().unwrap());
        let material = materials.add(StandardMaterial {
            base_color: Color::WHITE,
            ..default()
        });

        // create a sphere
        commands.spawn((PbrBundle {
            mesh,
            material,
            transform: Transform::from_xyz(0.0, 2.0, 0.0)
                .with_rotation(Quat::from_rotation_x(-PI / 4.)),
            ..default()
        },));

        // spawn a light source

        commands.spawn(PointLightBundle {
            point_light: PointLight {
                intensity: 9000.0,
                range: 100.,
                shadows_enabled: true,
                ..default()
            },
            transform: Transform::from_xyz(8.0, 16.0, 8.0),
            ..default()
        });

        // spawn the camera
        commands.spawn(Camera3dBundle {
            projection: Projection::Perspective(PerspectiveProjection {
                fov: PI / 2.0, // 90 degrees
                near: 0.1,
                far: 1000.0,
                aspect_ratio: 1.0,
            }),
            transform: Transform::from_xyz(0.0, 6., 12.0)
                .looking_at(Vec3::new(0., 1., 0.), Vec3::Y),
            ..default()
        });
    }
}
